# Web extension

Sauteed onions associate registered domain names with onion addresses.  These
associations are established in CA-issued and CT-logged TLS certificates,
thereby making them publicly enumerable and hard to tamper with.

This repository contains a web extension that creates an onion-location redirect
prompt using sauteed onions.

## Setup

System that this web extension was developed on:

    $ lsb_release -a
    No LSB modules are available.
    Distributor ID: Debian
    Description:    Debian GNU/Linux 10 (buster)
    Release:        10
    Codename:       buster

    $ npm -v
    6.14.15

    $ node --version
    v14.18.2

    $ vue --version
    @vue/cli 4.5.15

    $ web-ext --version
    6.6.0

Pay attention that the Web extension is currently very sensitive to
dependencies' versions. It's also known to work with more recent versions of
[NPM](https://npmjs.com), such as the one provided by Debian bullseye (7.5.2).

Web extension template was generated based on instructions
[here](https://github.com/Kocal/vue-web-extension/tree/v1).

The important parts to get started:

    $ vue init kocal/vue-web-extension#v1 webext
    $ cd webext
    $ npm install

At some point we should fix the warnings that `npm install` cries about. Any
help from someone that is more familiar with this environment would be helpful.

## Development

Terminal one:

    npm run watch:dev

Terminal two:

    web-ext run --browser-console -s dist

If you use [firejail][https://github.com/netblue30/firejail], you may need to
use the `--firefox` option into the `web-ext` invocation to bypass it, such as:

    web-ext run --browser-console -s dist --firefox /usr/bin/firefox-esr

You can also install temporarily in Firefox as follows:

1. `npm run build:dev && npm run build-zip`
2. Go to about:debugging ->
   This Firefox ->
   Load Temporary Add-on ->
   specify the generated zip file in `dist-zip`

Example on how to add a new dependency:

    npm install asn1js

## Testing

Before testing the extension, make sure to:

1. Have Tor installed on your system.
2. Set Tor as the SOCKS5 proxy in the network settings tab (using SOCKS5 for DNS
   queries will also not hurt, so check this option as well).

## More information

Refer to [www.sauteed-onions.org][] for quick-start instructions, an FAQ, and
our pre-printed paper.

[www.sauteed-onions.org]: https://www.sauteed-onions.org
